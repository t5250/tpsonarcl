package fds.tp2;
import poste.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestColis {
	
	String origine3 = "France";
	String origine4 = "Pologne";

    String destination3 = "Mayotte";
    String destination4 = "Martinique";

    String codePostal3 = "34444";
    String codePostal4 = "333333";

    float poids3 = 60f;
    float poids4 = 2f;

    float volume3 = 12f;
    float volume4 = 1f;

    Recommandation r3 = Recommandation.zero;
    Recommandation r4 = Recommandation.un;

    String declareContenu3 = "un Benoit";
    String declareContinu4 = "un micro";

    float valeurDeclaree3 = 5.3f;
    float valeurDeclaree4 = 5.2f;
    
	Colis c1;
	Colis c2;
	Colis c3;
	Colis c4;
	
	@BeforeEach
	public void setUp() {
		c1=new Colis();
		c2=new Colis();
        c3 = new Colis(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3);
        c4 = new Colis(origine4, destination4, codePostal4, poids4, volume4, r4, declareContinu4, valeurDeclaree4);
	}
	
	@Test
	public void testGetDeclareContenu() {
		assertEquals("vide",c1.getDeclareContenu());
		assertEquals("vide",c2.getDeclareContenu());
		assertEquals("un Benoit",c3.getDeclareContenu());
		assertEquals("un micro",c4.getDeclareContenu());
	}
	
	@Test
	public void testGetValeurDeclaree() {
		assertEquals(0,c1.getValeurDeclaree());
		assertEquals(0,c2.getValeurDeclaree());
		assertEquals(5.3f,c3.getValeurDeclaree());
		assertEquals(5.2f,c4.getValeurDeclaree());
	}
	@Test
	public void testTarifRemboursement() {
		assertEquals(0,c1.tarifRemboursement());
		assertEquals(0,c2.tarifRemboursement());
		assertEquals(0,c3.tarifRemboursement());
		assertEquals(0.52f,c4.tarifRemboursement());
	}
	@Test
	public void testTarifAffranchissement() {
		assertEquals(2,c1.tarifAffranchissement());
		assertEquals(2,c2.tarifAffranchissement());
		assertEquals(5,c3.tarifAffranchissement());
		assertEquals(5.5,c4.tarifAffranchissement());
		
	}
	@Test
	public void testToString() {
		assertEquals("Colis 0000/inconnue/0/0.0/0.0",c1.toString());
		assertEquals("Colis 0000/inconnue/0/0.0/0.0",c2.toString());
		assertEquals("Colis 34444/Mayotte/0/12.0/5.3",c3.toString());
		assertEquals("Colis 333333/Martinique/1/1.0/5.2",c4.toString());
		
	}
	@Test
	public void testTypeObjetPostal() {
		assertEquals("Colis",c1.typeObjetPostal());
		assertEquals("Colis",c2.typeObjetPostal());
		assertEquals("Colis",c3.typeObjetPostal());
		assertEquals("Colis",c4.typeObjetPostal());
		
	}
}	


