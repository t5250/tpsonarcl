package fds.tp2;

import poste.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;

public class TestCourriel {
	
	
	Courriel c1;
    Courriel c2;
    Courriel c3;
    Courriel c4;

    @BeforeEach
    public void setUp() {
        ArrayList<String> pieces= new ArrayList<String>();
        pieces.add("p1");
        pieces.add("p2");
        c1 = new Courriel("elkfekf","jesaispas", "message kef", pieces);
        c2 = new Courriel("ma@mef.fr","jesaispas", "eifh message", pieces);
        c3 = new Courriel("@fehoi.fr","", "message pièce", pieces);
        c4 = new Courriel("@iefh"," ", "message PJ", pieces);
    }
    
    
	@ParameterizedTest
    @ValueSource(strings = {"adresses", "jspme.fr","@ihfep.fr"})
    void testEmailParametreFalseEmail(String adresse) {
        c1.setAdresseMail(adresse);
        assertEquals(false,c1.verifAdrEmail());
    }

	 @ParameterizedTest(name = "{index} - {0} should return {1}")
	 @MethodSource("destinationTest")
	 void destinTest(String adress, boolean expected, Courriel c1) {
		 c1.setAdresseMail(adress);
	     assertEquals(expected, c1.verifAdrEmail());
	  }

	 private static Stream<Arguments> destinationTest(){
		 ArrayList<String> pieces= new ArrayList<String>();
	     pieces.add("p1");
	     pieces.add("p2");
	     Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
	     return Stream.of(
	    		 Arguments.of("adres@ses.ihefih", true,c),
	             Arguments.of("adresses.fr", false,c),
	             Arguments.of("adresses", false,c),
	             Arguments.of("@me.fr", false,c),
	             Arguments.of("fr@ihfep.fr", true,c),
	             Arguments.of("uefh@iepf.fr", true,c)
	             );
	 }
 
}
