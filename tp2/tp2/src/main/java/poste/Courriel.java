package poste;
import java.util.ArrayList;

public class Courriel {
	private String adresseMail;
	private String titreMail;
	private String message;
	private ArrayList<String> piecesJointes;
	
	public Courriel() {
		this.adresseMail="null@null.null";
		this.titreMail="";
		this.message="";
		this.piecesJointes= new ArrayList<String>();
	}
	public Courriel(String adresseMail, String titreMail, String message, ArrayList<String> piecesJointes) {
		this.adresseMail=adresseMail;
		this.titreMail=titreMail;
		this.message=message;
		this.piecesJointes= piecesJointes;
	}
	
	public String getAdresseMail() {
		return adresseMail;
	}
	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}
	public String getTitreMail() {
		return titreMail;
	}
	public void setTitreMail(String titreMail) {
		this.titreMail = titreMail;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ArrayList<String> getPiecesJointes() {
		return piecesJointes;
	}
	public void setPiecesJointes(ArrayList<String> piecesJointes) {
		this.piecesJointes = piecesJointes;
	}
	
	public boolean verifAdrEmail() {
		String pattern="^(.+)@(.+)$";
		return  adresseMail.matches(pattern);
	}
	public boolean piecesJointesVide(){
		return piecesJointes.get(0).isEmpty();
	}
	public boolean messageContient() {
		String[] temp=message.split(" ");
		int tailleTemp=temp.length;
		String[] mots= {"PJ", "pièce","joints","joint","pièces","PJs","jointe","jointes"};
		int tailleMots=mots.length;
		for(int i=0; i<tailleTemp;i++) {
			for (int j=0;j<tailleMots;j++) {
				if (temp[i]==mots[j]) {
					return true;
				}
			}
		}
		return false;
	}
}
