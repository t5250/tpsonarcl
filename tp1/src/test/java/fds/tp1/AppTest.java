package fds.tp1;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
	ClasseMachin objet= new ClasseMachin("truc","chose","gros",15);
    @Test
    public void shouldAnswerWithTrue()
    {
        assertEquals("truc", objet.getNom());
    }
}
