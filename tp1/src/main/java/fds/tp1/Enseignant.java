package fds.tp1;

import java.util.concurrent.atomic.AtomicInteger;

public class Enseignant {
	private static final AtomicInteger ID_FACTORY=new AtomicInteger();
	private final int id;
	private String nom;
	private String prenom;
	
	public Enseignant() {
		this.id=ID_FACTORY.getAndIncrement();
		this.nom="nom";
		this.prenom="prenom";
	}
	public Enseignant(String nom, String prenom) {
		this.id=ID_FACTORY.getAndIncrement();
		this.nom=nom;
		this.prenom=prenom;
	}
	public int getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
