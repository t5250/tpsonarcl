package fds.tp1;

import java.util.concurrent.atomic.AtomicInteger;

public class Sujet {
	private static final AtomicInteger ID_FACTORY=new AtomicInteger();
	private final int id;
	public String titre;
	public String resume;

	public Sujet() {
		this.id=ID_FACTORY.getAndIncrement();
		this.titre="titre";
		this.resume="resume";
	}
	public Sujet(String titre, String resume) {
		this.id=ID_FACTORY.getAndIncrement();
		this.titre=titre;
		this.resume=resume;
	}
	public int getId() {
		return id;
	}
	public String getTitre() {
		return titre;
	}
	public void setSujet(String titre) {
		this.titre = titre;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}

}
