package fds.tp1;

public class ClasseMachin {
	private String nom;
	private String prenom;
	private String description;
	private int age;
	
	public ClasseMachin() {
		this.nom="Nyme";
		this.prenom="Ano";
		this.description="vide";
		this.age=0;
	}
	public ClasseMachin(String n, String p,String d, int a){
		this.nom=n;
		this.prenom="p";
		this.description="d";
		this.age=a;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public char lettrePosition(String nom, int position) {
		return nom.charAt(position-1);
	}
	
}
