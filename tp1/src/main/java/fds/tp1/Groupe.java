package fds.tp1;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
public class Groupe {
	private static final AtomicInteger ID_FACTORY=new AtomicInteger();
	private final int id;
	private ArrayList<Etudiant> membres = new ArrayList<Etudiant>(5);
	
	
	public Groupe(){
		this.id=ID_FACTORY.getAndIncrement();
	}
	public Groupe(Etudiant idRepresentant, ArrayList<Etudiant> membres) {
		this.id=ID_FACTORY.getAndIncrement();
		this.membres = membres;
	}
	public int getId() {
		return id;
	}
	public Etudiant getRepresentant() {
		return getMembreIndex(0);
	}
	public ArrayList<Etudiant> getMembres() {
		return membres;
	}
	public void setMembres(ArrayList<Etudiant> membres) {
		this.membres = membres;
	}
	public Etudiant getMembreIndex(int index) {
		return membres.get(index);
	}
}
